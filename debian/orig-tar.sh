#!/bin/sh -e

TAR=../maven-ant-tasks_$2.orig.tar.gz
DIR=maven-ant-tasks-$2
TAG=maven-ant-tasks-$2

svn export http://svn.apache.org/repos/asf/maven/ant-tasks/tags/$TAG $DIR
tar -c -z -f $TAR $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
